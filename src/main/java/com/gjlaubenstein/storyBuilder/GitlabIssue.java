package com.gjlaubenstein.storyBuilder;

import java.util.ArrayList;

public class GitlabIssue {
    private String id;
    private String title;
    private ArrayList<Integer> assigneeIds;
    private Integer assigneeId;
    private Integer projectId;
    private Integer authorId;
    private String description;
    private String action;

    public void setId(String id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAssigneeIds(ArrayList<Integer> assigneeIds) {
        this.assigneeIds = assigneeIds;
    }

    public void setAssigneeId(Integer assigneeId) {
        this.assigneeId = assigneeId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public void setAuthorId(Integer authorId) {
        this.authorId = authorId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public ArrayList<Integer> getAssigneeIds() {
        return assigneeIds;
    }

    public Integer getAssigneeId() {
        return assigneeId;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public Integer getAuthorId() {
        return authorId;
    }

    public String getDescription() {
        return description;
    }

    public String getAction() {
        return action;
    }
}
