package com.gjlaubenstein.storyBuilder;

public class Repository {
    private String url;
    private String name;

    public Repository() {}

    public Repository(String url, String name) {
        this.url = url;
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }
}
