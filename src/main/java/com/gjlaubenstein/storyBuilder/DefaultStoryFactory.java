package com.gjlaubenstein.storyBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

@Component
public class DefaultStoryFactory implements StoryFactory {

    @Autowired
    MarkdownToTextConverter markdownToTextConverter;

    @Override
    public void createStory(GitlabIssue issue, Repository repository) throws IOException {
        Path storyPath =
                buildStoryPath(repository.getName(), issue.getTitle());

        List<String> lines = parseDescription(issue.getDescription());
        Files.write(storyPath, lines, Charset.forName("UTF-8"));
    }

    private Path buildStoryPath(String repoName, String storyTitle) {
        String camelCaseStoryTitle = camelCaseStoryTitle(storyTitle);
        return Paths.get(String.format("./%s/src/test/resources/stories/%s.story", repoName, camelCaseStoryTitle));
    }

    private List<String> parseDescription(String description) {
        String parsedDescription = markdownToTextConverter.convert(description);
        return Arrays.asList(parsedDescription.split(" {2}|\\n"));
    }

    private String camelCaseStoryTitle(String sentanceTitle) {
        return  Arrays.stream(sentanceTitle.split(" "))
                .map(String::toCharArray)
                .peek((char[] word) -> word[0] = Character.toUpperCase(word[0]))
                .map(String::new)
                .reduce("", (title, word) -> title + word);
    }
}
