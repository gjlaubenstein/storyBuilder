package com.gjlaubenstein.storyBuilder;

import java.io.IOException;

public interface StoryFactory {
    public void createStory(GitlabIssue issue, Repository repository) throws IOException;
}
