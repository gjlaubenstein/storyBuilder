package com.gjlaubenstein.storyBuilder;

import org.springframework.stereotype.Component;

@Component
public class MarkdownToTextConverter  {

    public String convert(String markdown) {
        return markdown
                .replaceAll("\\*\\*", "")
                .replaceAll("\\*\\*\\*", "");
    }

}
