package com.gjlaubenstein.storyBuilder;

public class WebhookRequest {
    private GitlabIssue object_attributes;
    private Repository repository;
    private Project project;

    public WebhookRequest() {

    }

    public WebhookRequest(GitlabIssue object_attributes, Repository repository, Project project) {
        this.object_attributes = object_attributes;
        this.repository = repository;
        this.project = project;
    }

    public GitlabIssue getObject_attributes() {
        return object_attributes;
    }

    public Repository getRepository() {
        return repository;
    }

    public Project getProject() {
        return project;
    }
}
