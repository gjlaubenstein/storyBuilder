package com.gjlaubenstein.storyBuilder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StoryBuilderApplication {

	public static void main(String[] args) {
		SpringApplication.run(StoryBuilderApplication.class, args);
	}
}
