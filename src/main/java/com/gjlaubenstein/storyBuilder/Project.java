package com.gjlaubenstein.storyBuilder;

public class Project {
    private String git_http_url;
    private String http_url;

    public Project() {}

    public Project(String git_http_url, String http_url) {
        this.git_http_url = git_http_url;
        this.http_url = http_url;
    }

    public String getGit_http_url() {
        return git_http_url;
    }

    public String getHttp_url() {
        return http_url;
    }
}
