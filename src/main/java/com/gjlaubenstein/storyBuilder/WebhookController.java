package com.gjlaubenstein.storyBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;

@RestController
public class WebhookController {
    private final String PUBLISH_SCRIPT = "./scripts/publish.sh";

    @Autowired
    ShellExecutor shellExecutor;


    @Autowired
    StoryFactory storyFactory;

    public WebhookController() throws IOException {
    }

    @PostMapping("/issueEvents")
    public void consumeWebhook(@RequestBody WebhookRequest request) throws IOException, InterruptedException {
        GitlabIssue gitlabIssue = request.getObject_attributes();
        String branchName = String.format("#%s-%s", gitlabIssue.getId(), gitlabIssue.getTitle().replaceAll(" ", "-"));
        String repoName = request.getRepository().getName();
        shellExecutor.executeCommand(String.format("git clone %s", request.getProject().getGit_http_url()));
        storyFactory.createStory(request.getObject_attributes(), request.getRepository());
        shellExecutor.executeCommand(String.format("%s %s %s", PUBLISH_SCRIPT, repoName, branchName));
    }
}
