Meta:

**Narrative:**  
As a API User  
I want to Update a Dog  
So that I can add a medical record

**Scenario:** update a dog's medical record as a API User  
**Given** an existing dog  
**When** I update the dog's medical history   
**Then** the new medical record is attached to the dog
