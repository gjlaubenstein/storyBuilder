package com.gjlaubenstein.storyBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.file.Files;
import java.nio.file.Paths;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(WebhookController.class)
@AutoConfigureMockMvc
public class WebhookControllerTest {

    @MockBean
    private ShellExecutor shellExecutor;

    @MockBean
    private MarkdownToTextConverter markdownToTextConverter;

    @MockBean
    private StoryFactory storyFactory;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void itProperlyParsesTheWebhook() throws Exception {
        ClassPathResource story = new ClassPathResource("story.md");
        String description = new String(Files.readAllBytes(Paths.get(story.getURI())));
        GitlabIssue gitlabIssue = new GitlabIssue();
        gitlabIssue.setDescription(description);
        Repository repository = new Repository("a-url", "a-name");
        WebhookRequest webhookRequest = new WebhookRequest(gitlabIssue, repository);

        ObjectMapper objectMapper = new ObjectMapper();
        String requestBody = objectMapper.writeValueAsString(webhookRequest);

        when(markdownToTextConverter.convert(anyString()))
                .thenReturn("yay");

        when(shellExecutor.executeCommand(anyString()))
                .thenReturn("result");

        doNothing().when(storyFactory).createStory(any(GitlabIssue.class), any(Repository.class));

        mockMvc.perform(post("/issueEvents")
                .content(requestBody)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }
}
