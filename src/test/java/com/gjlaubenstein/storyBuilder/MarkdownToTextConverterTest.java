package com.gjlaubenstein.storyBuilder;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@RunWith(SpringRunner.class)
public class MarkdownToTextConverterTest {
    private String description;

    @Before
    public void setup() throws IOException {
        ClassPathResource story = new ClassPathResource("story.md");
        description = new String(Files.readAllBytes(Paths.get(story.getURI())));
    }

    @Test
    public void itRemovesMarkdownSyntax() {
        String expectedValue = "Meta:\n" +
                "\n" +
                "Narrative:  \n" +
                "As a API User  \n" +
                "I want to Update a Dog  \n" +
                "So that I can add a medical record\n" +
                "\n" +
                "Scenario: update a dog's medical record as a API User  \n" +
                "Given an existing dog  \n" +
                "When I update the dog's medical history   \n" +
                "Then the new medical record is attached to the dog\n";
        MarkdownToTextConverter markdownToTextConverter = new MarkdownToTextConverter();
        String actualValue = markdownToTextConverter.convert(description);
        Assert.assertEquals(expectedValue, actualValue);
    }
}
