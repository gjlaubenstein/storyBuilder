#!/usr/bin/env bash
cd $1
git checkout -b $2
git add .
git commit -m "Add acceptance tests"
git push -u origin $2
