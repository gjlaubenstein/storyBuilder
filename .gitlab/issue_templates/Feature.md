Meta:

**Narrative:**  
As a [casual API User, enterprise API User, Web UI User]  
I want to [perform an action]  
So that I [can achieve a business goal]

**Scenario:** [scenario description]  
**Given** a system state   
**When** I do something   
**Then** system is in a different state  
